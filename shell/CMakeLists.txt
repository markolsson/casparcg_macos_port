cmake_minimum_required (VERSION 2.6)
project (shell)

join_list("${CASPARCG_MODULE_INCLUDE_STATEMENTS}" "\n" CASPARCG_MODULE_INCLUDE_STATEMENTS)
join_list("${CASPARCG_MODULE_INIT_STATEMENTS}" "\n" CASPARCG_MODULE_INIT_STATEMENTS)
join_list("${CASPARCG_MODULE_UNINIT_STATEMENTS}" "\n" CASPARCG_MODULE_UNINIT_STATEMENTS)
join_list("${CASPARCG_MODULE_COMMAND_LINE_ARG_INTERCEPTORS_STATEMENTS}" "\n" CASPARCG_MODULE_COMMAND_LINE_ARG_INTERCEPTORS_STATEMENTS)
configure_file("${PROJECT_SOURCE_DIR}/included_modules.tmpl" "${PROJECT_SOURCE_DIR}/included_modules.h")

set(OS_SPECIFIC_SOURCES
	linux_specific.cpp
)

set(SOURCES
	casparcg.config
	included_modules.tmpl
	main.cpp
	server.cpp
	stdafx.cpp
)
set(HEADERS
	default_audio_config.h
	included_modules.h
	platform_specific.h
	server.h
	stdafx.h
)
SET(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/bin)

add_executable(casparcg MACOSX_BUNDLE ${SOURCES} ${HEADERS} ${OS_SPECIFIC_SOURCES})
add_precompiled_header(casparcg stdafx.h FORCEINCLUDE)

FIX_MACOSX_MAIN_FRAMEWORK_RPATH(casparcg)

include_directories(..)
include_directories(${BOOST_INCLUDE_PATH})
include_directories(${RXCPP_INCLUDE_PATH})
include_directories(${TBB_INCLUDE_PATH})
include_directories(${X11_INCLUDE_DIR})

source_group(sources ./*)

if (MSVC)
	target_link_libraries(casparcg
		zlibstat.lib
	)
endif ()

target_link_libraries(casparcg
	accelerator
	common
	core
	protocol
	"${CASPARCG_MODULE_PROJECTS}"
)

add_custom_target(casparcg_copy_dependencies ALL)

casparcg_add_runtime_dependency("${CMAKE_CURRENT_SOURCE_DIR}/casparcg.config")
casparcg_add_runtime_dependency("${CMAKE_BINARY_DIR}/external/caspar_sample_data/font")
casparcg_add_runtime_dependency("${CMAKE_BINARY_DIR}/external/caspar_sample_data/media")
casparcg_add_runtime_dependency("${CMAKE_BINARY_DIR}/external/caspar_sample_data/template")

set(OUTPUT_FOLDER ${EXECUTABLE_OUTPUT_PATH})
add_custom_command(TARGET casparcg_copy_dependencies POST_BUILD COMMAND ${CMAKE_COMMAND} -E make_directory \"${OUTPUT_FOLDER}\")

foreach(FILE_TO_COPY ${CASPARCG_RUNTIME_DEPENDENCIES})
if(IS_DIRECTORY ${FILE_TO_COPY})
	get_filename_component(FOLDER_NAME "${FILE_TO_COPY}" NAME)
	add_custom_command(TARGET casparcg_copy_dependencies POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_directory \"${FILE_TO_COPY}\" \"${OUTPUT_FOLDER}/${FOLDER_NAME}\")
	add_custom_command(TARGET casparcg_copy_dependencies POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_directory \"${FILE_TO_COPY}\" \"${CMAKE_CURRENT_BINARY_DIR}/${FOLDER_NAME}\")
else()
	add_custom_command(TARGET casparcg_copy_dependencies POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy \"${FILE_TO_COPY}\" \"${OUTPUT_FOLDER}/\")
	add_custom_command(TARGET casparcg_copy_dependencies POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy \"${FILE_TO_COPY}\" \"${CMAKE_CURRENT_BINARY_DIR}/\")
endif()
endforeach(FILE_TO_COPY)

add_custom_command(TARGET casparcg_copy_dependencies POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_directory \"${CMAKE_BINARY_DIR}/external/cef/Release/Chromium Embedded Framework.framework/\" \"${EXECUTABLE_OUTPUT_PATH}/casparcg.app/Contents/Frameworks/Chromium Embedded Framework.framework\")

INSTALL(CODE "
   include(BundleUtilities)
   fixup_bundle(\"${EXECUTABLE_OUTPUT_PATH}/casparcg.app\"   \"\"   \"${EXTERNAL_LIB_PATH}\")
   " COMPONENT Runtime)

INSTALL(DIRECTORY "${CMAKE_BINARY_DIR}/modules/html/Release/casparcg Helper.app" DESTINATION "${EXECUTABLE_OUTPUT_PATH}/casparcg.app/Contents/Frameworks" USE_SOURCE_PERMISSIONS)
