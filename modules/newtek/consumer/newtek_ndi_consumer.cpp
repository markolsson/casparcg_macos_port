/*
* Copyright 2013 NewTek
*
* This file is part of CasparCG (www.casparcg.com).
*
* CasparCG is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* CasparCG is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with CasparCG. If not, see <http://www.gnu.org/licenses/>.
*
* Author: Robert Nagy, ronag@live.com
* Author: Mark Olsson, post@markolsson.se
*/

#include "../StdAfx.h"

#include "newtek_ndi_consumer.h"

#include <core/consumer/frame_consumer.h>
#include <core/video_format.h>
#include <core/frame/frame.h>
#include <core/frame/audio_channel_layout.h>
#include <core/mixer/audio/audio_util.h>
#include <core/monitor/monitor.h>
#include <core/help/help_sink.h>
#include <core/help/help_repository.h>

#include <common/assert.h>
#include <common/executor.h>
#include <common/diagnostics/graph.h>
#include <common/timer.h>
#include <common/param.h>

#include <boost/algorithm/string.hpp>
#include <boost/timer.hpp>
#include <boost/property_tree/ptree.hpp>

#include <tbb/atomic.h>

#include <Processing.NDI.Lib.h>

#include "../util/air_send.h"
#include "../util/ndi_lib.h"

namespace caspar { namespace newtek {

struct newtek_ndi_consumer : public core::frame_consumer
{
	core::monitor::subject				monitor_subject_;
	NDIlib_send_instance_t				ndi_;
	core::video_format_desc				format_desc_;
	core::audio_channel_layout			channel_layout_		= core::audio_channel_layout::invalid();
	executor							executor_;
	tbb::atomic<bool>					connected_;
	spl::shared_ptr<diagnostics::graph>	graph_;
	timer								tick_timer_;
	timer								frame_timer_;

public:

	newtek_ndi_consumer()
		: executor_(print())
	{
		if (!ndi::is_available())
			CASPAR_THROW_EXCEPTION(not_supported() << msg_info(ndi::dll_name() + L" not available"));

		connected_ = false;

		graph_->set_text(print());
		graph_->set_color("frame-time", diagnostics::color(0.5f, 1.0f, 0.2f));
		graph_->set_color("tick-time", diagnostics::color(0.0f, 0.6f, 0.9f));
		graph_->set_color("dropped-frame", diagnostics::color(0.3f, 0.6f, 0.3f));
		diagnostics::register_graph(graph_);
	}

	~newtek_ndi_consumer()
	{
	}

	// frame_consumer

	virtual void initialize(
			const core::video_format_desc& format_desc,
			const core::audio_channel_layout& channel_layout,
			int channel_index) override
	{
		NDIlib_send_create_t NDI_send_create_desc;
		NDI_send_create_desc.p_ndi_name = "CasparCG Channel "; // Add channel index

		ndi_ = NDIlib_send_create(&NDI_send_create_desc);

		//CASPAR_VERIFY(ndi_);

		format_desc_	= format_desc;
		channel_layout_	= channel_layout;
	}

/*

				channel_layout.num_channels,
				format_desc.audio_sample_rate)
*/

	std::future<bool> schedule_send(core::const_frame frame)
	{
		return executor_.begin_invoke([=]() -> bool
		{
			graph_->set_value("tick-time", tick_timer_.elapsed() * format_desc_.fps * 0.5);
			tick_timer_.restart();
			frame_timer_.restart();

			// AUDIO

			auto audio_buffer = core::audio_32_to_16(frame.audio_data());

			//ndi::add_audio(ndi_.get(), audio_buffer.data(), static_cast<int>(audio_buffer.size()) / channel_layout_.num_channels);

			// VIDEO

			//connected_ = ndi::add_frame_bgra(ndi_.get(), frame.image_data().begin());

			NDIlib_video_frame_v2_t NDI_video_frame;
			NDI_video_frame.xres = format_desc_.width;
			NDI_video_frame.yres = format_desc_.height;
			NDI_video_frame.FourCC = NDIlib_FourCC_type_BGRA;
			NDI_video_frame.p_data = (uint8_t*)malloc(format_desc_.width * format_desc_.height * 4);
			NDI_video_frame.frame_rate_N = format_desc_.time_scale;
			NDI_video_frame.frame_rate_D = format_desc_.duration;
			NDI_video_frame.frame_format_type = (format_desc_.field_mode == core::field_mode::progressive) ? NDIlib_frame_format_type_progressive : NDIlib_frame_format_type_interleaved;
			NDI_video_frame.picture_aspect_ratio = static_cast<float>(format_desc_.square_width) / static_cast<float>(format_desc_.square_height);
			NDI_video_frame.line_stride_in_bytes = format_desc_.width * 4;

			NDI_video_frame.p_data = const_cast<uint8_t*>(frame.image_data().begin());

			NDIlib_send_send_video_v2(ndi_, &NDI_video_frame);

			graph_->set_text(print());
			graph_->set_value("frame-time", frame_timer_.elapsed() * format_desc_.fps * 0.5);

			return true;
		});
	}

	virtual std::future<bool> send(core::const_frame frame) override
	{
		CASPAR_VERIFY(format_desc_.height * format_desc_.width * 4 == frame.image_data().size());

		if (executor_.size() > 0 || executor_.is_currently_in_task())
		{
			graph_->set_tag(diagnostics::tag_severity::WARNING, "dropped-frame");

			return make_ready_future(true);
		}

		schedule_send(std::move(frame));

		return make_ready_future(true);
	}

	virtual core::monitor::subject& monitor_output() override
	{
		return monitor_subject_;
	}

	virtual std::wstring print() const override
	{
		return connected_ ?
				L"newtek-ndi[connected]" : L"newtek-ndi[not connected]";
	}

	virtual std::wstring name() const override
	{
		return L"newtek-ndi";
	}

	virtual boost::property_tree::wptree info() const override
	{
		boost::property_tree::wptree info;
		info.add(L"type", L"newtek-ndi-consumer");
		info.add(L"connected", connected_ ? L"true" : L"false");
		return info;
	}

	virtual int buffer_depth() const override
	{
		return -1;
	}

	virtual int index() const override
	{
		return 900;
	}

	virtual int64_t presentation_frame_age_millis() const override
	{
		return 0;
	}

	virtual bool has_synchronization_clock() const override
	{
		return false;
	}
};

void describe_ndi_consumer(core::help_sink& sink, const core::help_repository& repo)
{
	sink.short_description(L"A consumer for streaming a channel via the NDI protocol.");
	sink.syntax(L"NEWTEK_NDI");
	sink.para()->text(L"A consumer for streaming a channel via the NDI protocol.");
	sink.para()->text(L"Examples:");
	sink.example(L">> ADD 1 NEWTEK_NDI");
}

spl::shared_ptr<core::frame_consumer> create_ndi_consumer(const std::vector<std::wstring>& params, core::interaction_sink*, std::vector<spl::shared_ptr<core::video_channel>> channels)
{
	if (params.size() < 1 || !boost::iequals(params.at(0), L"NEWTEK_NDI"))
		return core::frame_consumer::empty();

	return spl::make_shared<newtek_ndi_consumer>();
}

spl::shared_ptr<core::frame_consumer> create_preconfigured_ndi_consumer(const boost::property_tree::wptree& ptree, core::interaction_sink*, std::vector<spl::shared_ptr<core::video_channel>> channels)
{
	return spl::make_shared<newtek_ndi_consumer>();
}

}}
