cmake_minimum_required (VERSION 2.6)
project (html)

if (POLICY CMP0037)
	cmake_policy(SET CMP0037 OLD)
endif ()

set(MODULE_TARGET "html")
set(CEF_HELPER_TARGET "casparcg Helper")

set(CEF_ROOT ${CMAKE_BINARY_DIR}/external/cef)
find_package(CEF)

set(SOURCES
		producer/html_cg_proxy.cpp
		producer/html_producer.cpp
		html.cpp
)
set(HEADERS
		producer/html_cg_proxy.h
		producer/html_producer.h
		html.h
)

include_directories(..)
include_directories(../..)
include_directories(${BOOST_INCLUDE_PATH})
include_directories(${RXCPP_INCLUDE_PATH})
include_directories(${TBB_INCLUDE_PATH})
include_directories(${CEF_ROOT})
include_directories(${CEF_ROOT}/include)
include_directories(${ASMLIB_INCLUDE_PATH})

add_subdirectory(${CEF_LIBCEF_DLL_WRAPPER_PATH} libcef_dll_wrapper)

# cefsimple helper sources.
set(CEFSIMPLE_HELPER_SRCS_MACOSX
process_helper_mac.cc
)
APPEND_PLATFORM_SOURCES(CEFSIMPLE_HELPER_SRCS)
source_group(cefsimple FILES ${CEFSIMPLE_HELPER_SRCS})

ADD_LOGICAL_TARGET("libcef_lib" "${CEF_LIB_DEBUG}" "${CEF_LIB_RELEASE}")

# Determine the target output directory.
SET_CEF_TARGET_OUT_DIR()

set(CEF_HELPER_APP "${MODULE_TARGET_OUT_DIR}/${CEF_HELPER_TARGET}.app")
add_executable(${CEF_HELPER_TARGET} MACOSX_BUNDLE ${CEFSIMPLE_HELPER_SRCS})
SET_EXECUTABLE_TARGET_PROPERTIES(${CEF_HELPER_TARGET})
add_dependencies(${CEF_HELPER_TARGET} libcef_dll_wrapper)
target_link_libraries(${CEF_HELPER_TARGET} libcef_lib libcef_dll_wrapper ${CEF_STANDARD_LIBS})
set_target_properties(${CEF_HELPER_TARGET} PROPERTIES
  MACOSX_BUNDLE_INFO_PLIST ${CMAKE_CURRENT_SOURCE_DIR}/mac/helper-Info.plist
  )

# Fix the framework rpath in the helper executable.
FIX_MACOSX_HELPER_FRAMEWORK_RPATH(${CEF_HELPER_TARGET})

add_library(${MODULE_TARGET} ${SOURCES} ${HEADERS})
set_target_properties(${MODULE_TARGET} PROPERTIES FOLDER modules)
source_group(sources\\producer producer/*)
source_group(sources ./*)

add_dependencies(${MODULE_TARGET} libcef_dll_wrapper "${CEF_HELPER_TARGET}")
target_link_libraries(${MODULE_TARGET} libcef_lib libcef_dll_wrapper ${CEF_STANDARD_LIBS})

casparcg_add_include_statement("modules/html/html.h")
casparcg_add_init_statement("html::init" "html")
casparcg_add_uninit_statement("html::uninit")
casparcg_add_command_line_arg_interceptor("html::intercept_command_line")
casparcg_add_module_project("html")
