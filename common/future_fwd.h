#pragma once

#include "forward.h"

FORWARD2(std, __1, template<typename> class future);
FORWARD2(std, __1, template<typename> class shared_future);
FORWARD2(std, __1, template<typename> class promise);
